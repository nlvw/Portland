#!/bin/bash
# Install options/roles should be UEFI, Standard System Utilities, OpenSSH Server

###########################################################################################################
# Get Sensitive Info / Needed input
###########################################################################################################

# get SSH public key for primary user
read -rp 'Desired SSH Port: ' sshPort

# get zfs pool name
read -rp 'ZFS Pool To Import (NAME): ' zPName

# get Username for primary User
read -rp 'Primary Username: ' user
read -rp 'Email to Send Notifications: ' email

# get SNMTP credentials
# https://app.mailjet.com/account/setup
read -rp 'SMTP User: ' smtpUser
read -rsp 'SMTP Password: ' smtpPass

###########################################################################################################
# Install Updates
###########################################################################################################
apt update -y
apt upgrade -y

# Install Random Needed packages / WebTools (ensures they are installed)
apt install -y unzip wget curl git

###########################################################################################################
# Setup Automatic updates
###########################################################################################################

# Install ubunta apt auto update package
apt install -y unattended-upgrades

# Set Repos to Auto Update From && Reboot Settings && Mail Notification Settings
cat <<'EOF' > /etc/apt/apt.conf.d/50unattended-upgrades
    Unattended-Upgrade::Allowed-Origins {
        "${distro_id}:${distro_codename}";
        "${distro_id}:${distro_codename}-security";
        "${distro_id}ESM:${distro_codename}";
        "${distro_id}:${distro_codename}-updates";
        "Docker:${distro_codename}";
    };
    Unattended-Upgrade::Mail "root";
    Unattended-Upgrade::MailOnlyOnError "false";
    Unattended-Upgrade::Remove-Unused-Dependencies "true";
    Unattended-Upgrade::Automatic-Reboot "true";
    Unattended-Upgrade::Automatic-Reboot-Time "03:00";
EOF

# Set Update Schedule and Behaviour
cat <<'EOF' > /etc/apt/apt.conf.d/20auto-upgrades
    APT::Periodic::Enable "1";
    APT::Periodic::Update-Package-Lists "1";
    APT::Periodic::Download-Upgradeable-Packages "0";
    APT::Periodic::Unattended-Upgrade "7";
    APT::Periodic::AutocleanInterval "21";
EOF

###########################################################################################################
# Setup Uncomplicated Firewall (ufw)
###########################################################################################################

# Install and reset/delete existing rules
apt install -y ufw
ufw reset

# Set firewall defaults
ufw default deny incoming
ufw default allow outgoing
ufw logging medium

# Allow/Limit default SSH port
ufw limit 22/tcp

# Enable Firewall
ufw enable -y
systemctl enable ufw

###########################################################################################################
# Setup SSH for main user
###########################################################################################################
mkdir "/home/$user/.ssh"
touch "/home/$user/.ssh/authorized_keys"
chown -R "$user:$user /home/$user/.ssh"
chmod 700 "/home/$user/.ssh"
chmod 600 "/home/$user/.ssh/authorized_keys"

# Change SSH Settings
sed -i  "Port 22/ c\Port ${sshPort}" /etc/ssh/sshd_config
sed -i  'PermitRootLogin/ c\PermitRootLogin no' /etc/ssh/sshd_config
sed -i  'PubkeyAuthentication/ c\PubkeyAuthentication yes' /etc/ssh/sshd_config
sed -i  'AuthorizedKeysFile/ c\AuthorizedKeysFile %h/.ssh/authorized_keys' /etc/ssh/sshd_config
sed -i  'PasswordAuthentication/ c\PasswordAuthentication no' /etc/ssh/sshd_config

# Enable PasswordAuthentication on the local network only
cat << EOF >> /etc/ssh/sshd_config

    # Allow Local Lan to login with clear text passwords
    Match address $(ip route get 8.8.8.8 | cut -d ' ' -f 3 | cut -d '.' -f 1-3).0/24
            PasswordAuthentication yes
EOF

# Make Firewall Changes
ufw limit "${sshPort}/tcp"
ufw reload

# Resart SSH Server to load new settings
systemctl restart sshd

###########################################################################################################
# Setup mail for notifications
###########################################################################################################

# Install Needed packages
apt install -y msmtp msmtp-mta mailutils s-nail

# Create log directories
mkdir /var/log/msmtp
sudo touch /var/log/msmtp.log
sudo chmod 666 /var/log/msmtp.log

# Add root and primary user to mail group
usermod -aG mail root
usermod -aG mail "$user"

# Set settings for msmtp to send mail
cat << EOF > /etc/msmtprc
    defaults
        tls on
        tls_starttls on
        tls_trust_file /etc/ssl/certs/ca-certificates.crt
        logfile /var/log/msmtp/msmtp.log
        aliases /etc/aliases
    account portland
        host in-v3.mailjet.com
        port 587
        auth login
        user $smtpUser
        password $smtpPass
        from portland@wolfereign.com
    account default : portland
EOF

# Set settings to point the sendmail command to msmtp
cat << EOF > /etc/mail.rc
    set sendmail="/usr/bin/msmtp -t"
EOF

# Set the root and default email alias
cat << EOF > /etc/aliases
    root: $email
    default: $email
EOF

###########################################################################################################
# Install ZFS and Impot an existing Pool
###########################################################################################################

# Ensure ZFS is installed && Pool is imported
apt install -y zfsutils-linux
zpool import "$zPName"

# Setup ZFS Notifications
cat << EOF > /etc/zfs/zed.d/zed.rc
    ZED_DEBUG_LOG="/tmp/zed.debug.log"
    ZED_EMAIL_ADDR="root"
    ZED_EMAIL_PROG="mail"
    ZED_EMAIL_OPTS="-s '@SUBJECT@' @ADDRESS@"
    ZED_LOCKDIR="/var/lock"
    ZED_NOTIFY_INTERVAL_SECS=3600
    ZED_NOTIFY_VERBOSE=1
    ZED_RUNDIR="/var/run"
    #ZED_SPARE_ON_CHECKSUM_ERRORS=10
    #ZED_SPARE_ON_IO_ERRORS=1
    ZED_SYSLOG_PRIORITY="daemon.notice"
    ZED_SYSLOG_TAG="zed"
EOF

# Enable zfs notification service
systemctl enable zed
systemctl start zed

###########################################################################################################
# Install Docker
###########################################################################################################

# Ensure need packages are installed
apt install -y apt-transport-https ca-certificates curl software-properties-common

# Add Official Docker Repo and Install Docker Community Edition
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update
apt install -y docker-ce

# Ensure Docker volume exists on the ZFS Pool
if [ ! -d "/${zPName}/docker" ]; then
    zfs create "${zPName}/docker"
fi

# Ensure docker service is stopped
systemctl stop docker

# Change Dockers Storage to ZFS Pool
mkdir /etc/systemd/system/docker.service.d
touch /etc/systemd/system/docker.service.d/docker.conf
cat << EOF >> /etc/systemd/system/docker.service.d/docker.conf
    [Service]
    ExecStart=
    ExecStart=/usr/bin/dockerd --graph=/${zPName}/docker --storage-driver=zfs
EOF

# Enable ipv6 (change the ipvx cidr to a unique private subnet)
#cat << EOF >> /etc/docker/daemon.json
#    {
#        "ipv6": true,
#        "fixed-cidr-v6": "2001:db8:1::/64"
#    }
#EOF

# Start and Enable docker service
systemctl daemon-reload
systemctl start docker
systemctl enable docker

# Setup Docker MacVLan (add ipv6 later)
docker network create -d macvlan  \
    --subnet="$(ip route get 8.8.8.8 | cut -d ' ' -f 3 | cut -d '.' -f 1-3).0/24"  \
    --ip-range="$(ip route get 8.8.8.8 | cut -d ' ' -f 3 | cut -d '.' -f 1-3).32/27"  \
    --gateway="$(ip route get 8.8.8.8 | cut -d ' ' -f 3)"  \
    -o parent="$(ip route get 8.8.8.8 | cut -d ' ' -f 5 | head -1)" macvlan-ipv4

###########################################################################################################
# Install KVM/QEMU ( I don't Need it so its commented out )
###########################################################################################################

# Install packages
#apt install -y qemu-kvm libvirt-bin bridge-utils virtinst cpu-checker

# Start/Enable libvirtd service
#systemctl enable libvirtd
#systemctl start libvirtd

# Add primary user ad KVM Manager
#usermod -aG libvirtd "$user"

# Configure Network Bridge


# Ensure ZFS directories exist
#if [ ! -d "/${zPName}/kvm" ]; then
#    zfs create "${zPName}/kvm"
#fi

# Set kvm storage location
#virsh pool-destroy default
#virsh pool-undefine default
#virsh pool-define-as --name zfsPool --source-name "${zPName}/kvm" --type zfs
#virsh pool-autostart zfsPool
#virsh pool-start zfsPool

###########################################################################################################
# Setup curator user for container/file operations
###########################################################################################################

# Create media library user (curator)
useradd -u 6846 curator
echo curator:"$(openssl rand -base64 32)" | chpasswd

# Add primary user to the curator group
usermod -aG curator "$user"

###########################################################################################################
# Create Media Libraries
###########################################################################################################
if [ ! -d "/${zPName}/library" ]; then
    zfs create "${zPName}/library"
    mkdir "/${zPName}/library/books"
    mkdir "/${zPName}/library/os-isos"
    mkdir "/${zPName}/library/music"
    mkdir "/${zPName}/library/pictures"
    mkdir "/${zPName}/library/temp"
    mkdir "/${zPName}/library/temp/.incomplete"
    mkdir "/${zPName}/library/temp/autoTorrent"
    mkdir "/${zPName}/library/videos"
fi

# Fix permissions on Library SubVolume
chown -R curator:curator "/${zPName}/library"
chmod -R 770 "/${zPName}/library"
chmod -R g+s "/${zPName}/library"

###########################################################################################################
# Install Cockpit Web Gui
###########################################################################################################
apt install -y cockpit cockpit-networkmanager cockpit-storaged cockpit-system \
    cockpit-packagekit cockpit-docker cockpit-machines

# Open firewall port
ufw limit 9090/tcp

###########################################################################################################
# Cleanup SSH Firewall Rules
###########################################################################################################

# Remove standard ssh port
## If you did not set a new SSH port in the begging then this will break your SSH Connection
## This may interrupt the ssh session which is why it is at the end of the file.
ufw delete limit 22/tcp
ufw reload
