#!/bin/bash
# https://hub.docker.com/_/alpine/
# duckdns.org

# Variables to make some stuff more concise
fName="dyndns-duckdns"
sDir="/vpool/container-data/secrets"
fPath="${sDir}/${fName}"

if [ ! -f "$fPath" ]; then
  # Get Sensitive Information for DuckDNS Entry
  read -rsp 'Duck DNS Account Token: ' token
  read -rp 'Duck DNS Domains To Update (HostName1 HostName2 HostName3)' hNames

  # Generate bash script to update the dyndns entry
  echo "#!/bin/sh" > "$fPath"
  for hName in $hNames; do
      echo "echo url=\"https://www.duckdns.org/update?domains=${hName}&token=${token}&ip=\" | curl -k -K -" >> "$fPath"
  done

  # Make Script executable
  chmod 750 "$fpath"
  chmod +x "$fPath"
fi

# Stand Up New container
docker run -d  \
  --name "$fName" \
  --restart="unless-stopped" \
  --mount type=bind,source=/etc/localtime,target=/etc/localtime,readonly \
  --mount type=bind,source="$fPath",target="/etc/periodic/15min/${fName}",readonly \
  wolfereign/crond:latest
