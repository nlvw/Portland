#!/bin/bash
# https://hub.docker.com/_/alpine/
# https://domains.google.com/registrar

# Variables to make some stuff more concise
fName="dyndns-google"
sDir="/vpool/container-data/secrets"
fPath="${sDir}/${fName}"

if [ ! -f "$fPath" ]; then
  # Get Desired hostnames to update
  read -rp 'Google DynDNS Entries To Update (hostname1 hostname2 hostname3)' hNames

  # Generate bash script to update the dyndns entries
  echo "#!/bin/sh" > "$fPath"
  for hName in $hNames; do
    # Get Sensitive Information for Goggle Dynamic DNS Entry
    read -rsp "${hName} DNS Entry UserName: " uName
    read -rsp "${hName} DNS Entry Password: " uPass

    # Generate bash script to update the dyndns entry
    echo "echo url=\"https://${uName}:${uPass}@domains.google.com/nic/update?hostname=${hName}\" | curl -k -K -" >> "$fPath"
  done

  # Make Script executable
  chmod 750 "$fpath"
  chmod +x "$fPath"
fi

# Stand Up New container
docker run -d  \
  --name "$fName" \
  --restart="unless-stopped" \
  --mount type=bind,source=/etc/localtime,target=/etc/localtime,readonly \
  --mount type=bind,source="$fPath",target="/etc/periodic/15min/${fName}",readonly \
  wolfereign/crond:latest
